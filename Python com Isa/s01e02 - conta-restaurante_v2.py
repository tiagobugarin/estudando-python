from sys import exit

def conta_restaurante():

  qtd_pessoas = int(input("Quantas pessoas consumiram? "))
  if qtd_pessoas <= 0:
    print("Esse valor é inválido. Nem você foi na parada? Aí fica difícil!")
    sys.exit()

  valor_conta = float(input("Qual o valor da conta? ").replace(',','.'))
  if valor_conta <= 0:
    print("Esse valor é inválido. Que conta é essa que não existe?")
    sys.exit()

  taxa_servico = float(input("Qual o valor da taxa de serviço? ").replace(',','.'))
  if taxa_servico < 0 or taxa_servico > 100:
    print("Esse valor é inválido. Nem muito generosa nem mesquinha, encontre o equilíbrio!")
    sys.exit()

  media_pessoa = (valor_conta/qtd_pessoas)
  valor_final_pessoa = media_pessoa + (media_pessoa * taxa_servico / 100)
  txt_valor_final_pessoa = str(valor_final_pessoa).replace('.',',')

  print("Cada uma deve pagar " , txt_valor_final_pessoa , "reais")