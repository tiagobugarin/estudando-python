def responsabilidade_financeira():

  moradia_recomendado    = 30
  educacao_recomendado   = 20
  transporte_recomendado = 15

  renda_mes_total  = float(input("Renda mensal total: ").replace(',','.'))
  moradia_total    = float(input("Gastos totais com moradia: ").replace(',','.'))
  educacao_total   = float(input("Gastos totais com educação: ").replace(',','.'))
  transporte_total = float(input("Gastos totais com transporte: ").replace(',','.'))

  moradia_porcento    = '%.1f'%(moradia_total / renda_mes_total * 100)
  educacao_porcento   = '%.1f'%(educacao_total / renda_mes_total * 100)
  transporte_porcento = '%.1f'%(transporte_total / renda_mes_total * 100)

  moradia_ideal    = renda_mes_total * 0.30
  educacao_ideal   = renda_mes_total * 0.20
  transporte_ideal = renda_mes_total * 0.15

  if moradia_total > moradia_ideal:
    moradia_texto = "Seus gastos totais com moradia comprometem " + str(moradia_porcento) + " de sua renda total. O máximo recomendado é de 30%. Portanto, idealmente, o máximo de sua renda comprometida com moradia deveria ser de R$ " + str(moradia_ideal) + "."
  else:
    moradia_texto = "Seus gastos totais com moradia comprometem " + str(moradia_porcento) + " de sua renda total. O máximo recomendado é de 30%. Seus gastos estão dentro da margem recomendada."

  if educacao_total > educacao_ideal:
    educacao_texto = "Seus gastos totais com educação comprometem " + str(educacao_porcento) + " de sua renda total. O máximo recomendado é de 20%. Portanto, idealmente, o máximo de sua renda comprometida com educação deveria ser de R$ " + str(educacao_ideal) + "."
  else:
    educacao_texto = "Seus gastos totais com educação comprometem " + str(educacao_porcento) + " de sua renda total. O máximo recomendado é de 20%. Seus gastos estão dentro da margem recomendada."

  if transporte_total > transporte_ideal:
    transporte_texto = "Seus gastos totais com transporte comprometem " + str(transporte_porcento) + " de sua renda total. O máximo recomendado é de 15%. Portanto, idealmente, o máximo de sua renda comprometida com transporte deveria ser de R$ " + str(transporte_ideal) + "."
  else:
    transporte_texto = "Seus gastos totais com transporte comprometem " + str(transporte_porcento) + " de sua renda total. O máximo recomendado é de 15%. Seus gastos estão dentro da margem recomendada."

  print("\nDiagnóstico:")
  print(moradia_texto)
  print(educacao_texto)
  print(transporte_texto)