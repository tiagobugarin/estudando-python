def potencia(A,B):
	resultado = float(1)
	if B > 0:
		for i in range(1,B+1):
			resultado = resultado * A
		return resultado
	elif B < 0:
		for i in range(1,(B*-1)+1):
			resultado = resultado * A
		resultadoreduzido = 1 / resultado
		resultadofracao = f'1/{resultado}'
		return [resultadoreduzido, resultadofracao]
	else:
		resultado = 1
		return resultado

def prepotencia():
	resultado = float()
	A = int(input('Digite um número inteiro: '))
	B = int(input('Digite outro número inteiro: '))
	resultadofinal = potencia(A,B)
	if type(resultadofinal) is list:
		print(f'O resultado da potencia de base {A} com expoente {B} é {resultadofinal[0]}, ou, se preferir, {resultadofinal[1]}.')
	else:
		print(f'O resultado da potencia de base {A} com expoente {B} é {resultadofinal}.')
