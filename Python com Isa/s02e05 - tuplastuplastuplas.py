tupla = ('cascata', 'cardamomo', 420, 'pirlimpimpim', 13, 65535, 'origami', 'circus')
elemento = 420

def tupla1():
	if elemento in tupla:
		print(f'Sim, o elemento {elemento} está presente na lista {tupla} na posição {tupla.index(elemento)}')
	else:
		print(f'Não, o elemento {elemento} não está presente na lista {tupla}.')

def tupla2():
	comprimentotupla = len(tupla)
	meeiro = round(comprimentotupla / 2)
	metade1 = tupla[:meeiro]
	metade2 = tupla[meeiro:]
	print(f'Primeira metade: {metade1}')
	print(f'Segunda  metade: {metade2}')

def tupla3():
	posicaoelemento = tupla.index(elemento)
	novatupla = tupla[:posicaoelemento] + tupla[posicaoelemento+1:]
	print(f'Tupla original: {tupla}')
	print(f'Nova tupla :    {novatupla}')

def tupla4():
	tuplareversa = tupla[::-1]
	print(f'Tupla original: {tupla}')
	print(f'Tupla reversa : {tuplareversa}')