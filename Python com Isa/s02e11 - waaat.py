import numpy as np

t = int(input('Digite um tamanho pra este vetor: (número inteiro positivo) '))

vetor = np.random.rand(1,t)
vetor = np.multiply(vetor, 10)
vetor = np.rint(vetor)
print(vetor)
quantidade = np.count_nonzero(vetor == 0)
print(f'0 (zero) aparece {quantidade} vezes.')
