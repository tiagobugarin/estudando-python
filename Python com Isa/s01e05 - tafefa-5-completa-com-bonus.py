import pandas as pd
import matplotlib.pyplot as plt

tabela = pd.read_csv('~/Documentos/Python/Python com Isa/economias.csv')
paises = []
periodo = []
pais = ''
ano = ''
anoinicial = int()
anofinal = int()
primeiroano = int()
segundoano = int()
penultimoano = int()
ultimoano = int()

def deondevens():
    global paises
    paises.clear()
    for linha in tabela['País'].values:
        paises.append(tabela.loc[tabela['País'] == linha, 'País'].values[0])

def imprimepaises():
    deondevens()
    izao = ''
    for i in sorted(paises):
        izao += f'{i}, '
    print(f'Estes são os países disponíveis: {izao}.\n')

def quepaisehesse(verificar):
    deondevens()
    global pais
    pais = str(input('Informe um país: '))
    if verificar == 1 or str(verificar).lower() in ['sim', 's', 'yes', 'y']:
        listinha = []
        for i in paises:
            listinha.append(i.lower())
        if pais.lower() in listinha:
            return
        else:
            print(f'País {pais} não disponível.\n')
            quepaisehesse(verificar)

def alistamento():
    i = tabela.copy()
    del i['País']
    global periodo
    periodo.clear()
    for k in i.keys():
        periodo.append(k)
    global primeiroano
    global segundoano
    global penultimoano
    global ultimoano
    primeiroano = int(periodo[0])
    segundoano = int(periodo[1])
    penultimoano = int(periodo[len(periodo)-2])
    ultimoano = int(periodo[len(periodo)-1])

def queanoehesse():
    alistamento()
    global ano
    ano = str(input(f'Informe um ano entre {primeiroano} e {ultimoano}: '))
    if ano in periodo:
        pass
    else:
        print(f'Ano {ano} não disponível.\n')
        queanoehesse()

def queanocomeca():
    alistamento()
    global anoinicial
    anoinicial = int(input(f'Ano inicial entre {primeiroano} e {penultimoano}: '))
    if anoinicial in range(primeiroano, penultimoano + 1, 1):
        pass
    else:
        print(f'Ano {anoinicial} não disponível.\n')
        queanocomeca()

def queanotermina():
    alistamento()
    global anofinal
    anofinal = int(input(f'Ano final entre {segundoano} e {ultimoano}: '))
    if anofinal in range(segundoano, ultimoano + 1, 1):
        pass
    else:
        print(f'Ano {anofinal} não disponível.\n')
        queanotermina()

def faixadetempo():
    queanocomeca()
    queanotermina()
    global anoinicial
    global anofinal
    if anoinicial > anofinal:
        troca = anoinicial
        anoinicial = anofinal
        anofinal = troca
    if anoinicial == anofinal:
        print(f'Ano inicial e ano final são iguais. Vamos tentar de novo!\n')
        faixadetempo()

def pib():
    imprimepaises()
    quepaisehesse(1)
    queanoehesse()
    pib = tabela.loc[tabela['País'] == pais, ano].values[0]
    print(f'PIB {pais} em {ano}: US${pib} trilhões.')

def variacao():
    for linha in tabela['País'].values:
        valor2013 = tabela.loc[tabela['País'] == linha, '2013'].values[0]
        valor2020 = tabela.loc[tabela['País'] == linha, '2020'].values[0]
        pais = tabela.loc[tabela['País'] == linha, 'País'].values[0]
        variacao = round( ((valor2020 - valor2013) / valor2013) * 100, 2 )
        print(f'{pais:<13} teve uma variação de {variacao}% entre 2013 e 2020')

def evolucao():
    imprimepaises()
    quepaisehesse(1)
    alistamento()
    pibs = []
    anos = []
    for ano in periodo:
        pibs.append(tabela.loc[tabela['País'] == pais, str(ano)].values[0])
        anos.append(ano)
    plt.figure()
    plt.title(f'Evolução do PIB do {pais} entre 2013 e 2020')
    plt.xlabel('Anos')
    plt.ylabel('PIB (em trilhões de dólares)')
    plt.grid()
    plt.plot(anos, pibs, 'go--')
    plt.show()

def variacaopais():
    quepaisehesse(1)
    faixadetempo()
    valorinicial = tabela.loc[tabela['País'] == pais, str(anoinicial)].values[0]
    valorfinal = tabela.loc[tabela['País'] == pais, str(anofinal)].values[0]
    variacaopais = round( ((valorfinal - valorinicial) / valorinicial) * 100, 2 )
    print(f'{pais} teve uma variação de {variacaopais}% entre {anoinicial} e {anofinal}')

def comparacao():
    alistamento()
    plt.figure(figsize=(15,8))
    for linha in tabela['País'].values:
        pais = tabela.loc[tabela['País'] == linha, 'País'].values[0]
        pibs = []
        anos = []
        for ano in periodo:
            pibs.append(tabela.loc[tabela['País'] == pais, str(ano)].values[0])
            anos.append(ano)
        plt.plot(anos, pibs, 'o-', label=pais)
    plt.title(f'Evolução dos PIBs entre 2013 e 2020')
    plt.xlabel('Anos')
    plt.ylabel('PIB (em trilhões de dólares)')
    plt.legend()
    plt.grid()
    plt.show()

def detalhes():
    alistamento()
    plt.figure(figsize=(15,8))
    imprimepaises()
    print(f'Digite "todos" para adicionar todos os países.\nDigite "0" para encerrar a lista.')
    global pais
    listadepaises = []
    while True:
        quepaisehesse(0)
        if pais.lower() == 'todos':
            listadepaises.clear()
            listadepaises = paises
            print(listadepaises)
            break
        elif pais == '0':
            break
        else:
            listadepaises.append(pais)
    listinha = []
    for i in listadepaises:
        listinha.append(i.lower())
    for linha in tabela['País'].values:
        pais = tabela.loc[tabela['País'] == linha, 'País'].values[0]
        pibs = []
        anos = []
        if pais.lower() in listinha:
            for ano in periodo:
                pibs.append(tabela.loc[tabela['País'] == pais, str(ano)].values[0])
                anos.append(ano)
            plt.plot(anos, pibs, 'o-', label=pais)
    plt.title(f'Evolução dos PIBs entre 2013 e 2020')
    plt.xlabel('Anos')
    plt.ylabel('PIB (em trilhões de dólares)')
    plt.legend()
    plt.grid()
    plt.show()
