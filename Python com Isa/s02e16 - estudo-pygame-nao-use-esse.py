# Usando a biblioteca Pygame, escreva um programa que implemente uma interface para o gerenciamento de informações de hardware: memória, disco(s)(se houver mais de um), cpu:

import psutil
import platform
from datetime import datetime

def get_size(bytes, sufixo="B"):
    factor = 1024
    for unidade in ["", "K", "M", "G", "T", "P"]:
        if bytes < factor:
            return f"{bytes:.2f}{unidade}{sufixo}"
        bytes /= factor

def sysinfo():
    print(':o'*10, 'Sistema', ':o'*10)
    uname = platform.uname()
    print(f"Sistema: {uname.system}")
    print(f"Nome: {uname.node}")
    print(f"Lançamento: {uname.release}")
    print(f"Versão: {uname.version}")
    print(f"Máquina: {uname.machine}")
    print(f"Processador: {uname.processor}")
    print(':o'*10, 'Memória', ':o'*10)
    memoria = psutil.virtual_memory()
    print(f"Memória Total: {get_size(memoria.total)}")  # vou usar
    print(f"Memória Disponível: {get_size(memoria.available)}")
    print(f"Memória Usada: {get_size(memoria.used)}")
    print(f"Porcentagem: {memoria.percent}%")       # vou usar
    swap = psutil.swap_memory()
    print(f"Swap Total: {get_size(swap.total)}")
    print(f"Swap Livre: {get_size(swap.free)}")
    print(f"Swap Usada: {get_size(swap.used)}")
    print(f"Porcentagem: {swap.percent}%")
    print(':o'*10, 'CPU', ':o'*10)
    print("Núcleos Físicos:", psutil.cpu_count(logical=False))
    print("Total de Núcleos:", psutil.cpu_count(logical=True))
    cpufreq = psutil.cpu_freq()
    print(f"Frequência Máxima: {cpufreq.max:.2f}Mhz")
    print(f"Frequência Mínima: {cpufreq.min:.2f}Mhz")
    print(f"Frequência Atual: {cpufreq.current:.2f}Mhz")
    # print("Uso da CPU por Thread:")
    # for i, percentage in enumerate(psutil.cpu_percent(percpu=True, interval=1)):
    #     print(f"Núcleo {i}: {percentage}%")
    print(f"Uso Total da CPU: {psutil.cpu_percent()}%")     # vou usar
    print(':o'*10, 'Discos e Partições', ':o'*10)
    particoes = psutil.disk_partitions()
    quantas_particoes = 1
    for particao in particoes:
        if 'efi' in particao.mountpoint.lower():
            continue
        print(f"=== Dispositivo: {particao.device} ===")
        print(f"  Ponto de Montagem: {particao.mountpoint}")
        print(f"  Sistema de Arquivos: {particao.fstype}")
        try:
            uso_particao = psutil.disk_usage(particao.mountpoint)
        except PermissionError:
            continue
        print(f"  Tamanho Total: {get_size(uso_particao.total)}")
        print(f"  Usado: {get_size(uso_particao.used)}")
        print(f"  Livre: {get_size(uso_particao.free)}")
        print(f"  Porcentagem: {uso_particao.percent}%")
        quantas_particoes += 1
    # get IO statistics since boot
    disk_io = psutil.disk_io_counters()
    print(f"Total lido: {get_size(disk_io.read_bytes)}")
    print(f"Total escrito: {get_size(disk_io.write_bytes)}")

sysinfo()
