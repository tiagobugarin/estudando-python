def triangulo():
	lados = (int(input('Digite o primeiro lado do triângulo: ')),
			 int(input('Digite o segundo lado do triângulo:  ')),
			 int(input('Digite o terceiro lado do triângulo: '))
			)
	x, y, z = lados
	if (x < y + z) and (y < x + z) and (z < x + y):
		# print('ok')
		if (x == y == z):
			print('Este triângulo é equilátero.')
		elif (x == y) or (x == z) or (y == z):
			print('Este triângulo é isósceles.')
		elif (x != y) and (x != z) and (y != z):
			print('Este triângulo é escaleno.')
	else:
		print('Esses valores não formam um triângulo.')