def somaosimpares():
	somatorio = int()
	ene = int(input('Digite um número inteiro positivo maior que 1: '))
	incluindoene = ene + 1
	if ene <= 1:
		print('Era pra ser maior que um xará! Vamos lá, de novo...')
		exit(1)
	for i in range(1, incluindoene):
		if i % 2:
			somatorio += i
		else:
			continue
	print(f'A soma dos números ímpares entre 1 e {ene}, inclusive, é { somatorio}.')
