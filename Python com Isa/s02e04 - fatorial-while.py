def fatorial_while():
	ene = int(input("Digite um número positivo inteiro: "))
	fatorial = 1
	while ene > 0:
	    fatorial = fatorial * ene
	    ene = ene - 1
	print(f'Fatorial de {ene} é {fatorial}')