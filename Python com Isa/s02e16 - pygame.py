import pygame
import psutil

gameLoop = True
cor_fundo = [89, 152, 26, 255]
cor_base = [129, 182, 34, 255]
cor_acento = [236, 248, 127, 255]
cor_texto = [61, 85, 12]
janela_x = 800
janela_y = 480
w_caixa = janela_x - 100
y_caixa = janela_y / 8
y_texto = y_caixa + 20
calculo_razao = w_caixa / 100

pygame.init()
display = pygame.display.set_mode([janela_x, janela_y])
pygame.display.set_caption('Tá achando que eu entendi esse pygame?')
tipografia = pygame.font.SysFont("Calibri", 24)

if __name__ == '__main__':
    while gameLoop:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gameLoop = False

        display.fill(cor_fundo)

        memoria = psutil.virtual_memory()
        memoria_total = round(memoria.total/pow(1024, 3), 1)
        memoria_porcentagem = memoria.percent
        memoria_variavel = memoria_porcentagem * calculo_razao
        cpufreq = psutil.cpu_freq()
        cpu_porcentagem = psutil.cpu_percent()
        cpu_variavel = cpu_porcentagem * calculo_razao
        cpu_maxfreq = round((cpufreq.max/1000),1)
        particoes = psutil.disk_partitions()

        # caixa da memória
        memoria_base1 = pygame.Rect(50, 50, w_caixa, y_caixa)
        pygame.draw.rect(display, cor_base, memoria_base1)
        memoria_base2 = pygame.Rect(50, 50, memoria_variavel, y_caixa)
        pygame.draw.rect(display, cor_acento, memoria_base2)
        memoria_rotulo1 = tipografia.render(f"Memória em uso: {memoria_porcentagem}%", 1, cor_texto)
        display.blit(memoria_rotulo1, (60, y_texto))
        memoria_rotulomax1 = tipografia.render(f"Total: {memoria_total}GB", 1, cor_texto)
        display.blit(memoria_rotulomax1, (580, y_texto))

        # caixa da cpu
        cpu_base1 = pygame.Rect(50, 150, w_caixa, y_caixa)
        pygame.draw.rect(display, cor_base, cpu_base1)
        cpu_base2 = pygame.Rect(50, 150, cpu_variavel, y_caixa)
        pygame.draw.rect(display, cor_acento, cpu_base2)
        cpu_rotulo1 = tipografia.render(f"CPU em uso: {cpu_porcentagem}", 1, cor_texto)
        display.blit(cpu_rotulo1, (60, y_texto + 100))
        cpu_rotulomax1 = tipografia.render(f"Total: {cpu_maxfreq}GHz", 1, cor_texto)
        display.blit(cpu_rotulomax1, (580, y_texto + 100))

        # caixa dos armazenamentos
        quantas_particoes = 0
        for particao in particoes:
            if 'efi' in particao.mountpoint.lower():
                continue
            try:
                uso_particao = psutil.disk_usage(particao.mountpoint)
            except PermissionError:
                continue
            part_variavel = uso_particao.percent * calculo_razao
            part_total = round(uso_particao.total / pow(1024, 3),1)
            part_base1 = pygame.Rect(50, (250 + (100 * quantas_particoes)), w_caixa, y_caixa)
            pygame.draw.rect(display, cor_base, part_base1)
            part_base2 = pygame.Rect(50, (250 + (100 * quantas_particoes)), part_variavel, y_caixa)
            pygame.draw.rect(display, cor_acento, part_base2)
            part_rotulo1 = tipografia.render(f"Partição: {particao.device} (Uso: {uso_particao.percent}%)", 1, cor_texto)
            display.blit(part_rotulo1, (60, y_texto + (200 + (100 * quantas_particoes))))
            part_rotulomax1 = tipografia.render(f"Total: {part_total}GB", 1, cor_texto)
            display.blit(part_rotulomax1, (580, y_texto + (200 + (100 * quantas_particoes))))
            quantas_particoes += 1

        pygame.display.update()
        pygame.time.wait(300)
