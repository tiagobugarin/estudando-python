import matplotlib.pyplot as plt

periodo_arr = []
valor_arr = []

def progressao():

    valor_inicial = float(input("Valor inicial: R$ ").replace(',', '.'))
    rendimento_pp = float(input("Rendimento por período (%): ").replace(',', '.'))
    aporte_pp = float(input("Aporte a cada período: R$ ").replace(',', '.'))
    total_pp = int(input("Total de períodos: ").replace(',', '.'))
    print()

    periodo_atual = 1
    while periodo_atual <= total_pp:
        valor_inicial = (valor_inicial + (valor_inicial * (rendimento_pp / 100))) + aporte_pp
        print("Após", periodo_atual, "período(s), o montante será de R$", '%.2f' % valor_inicial, ".")
        periodo_arr.append(periodo_atual)
        valor_arr.append(valor_inicial)
        periodo_atual += 1

def desenho():
    progressao()
    plt.figure()
    plt.title("Progressão")
    plt.xlabel("Meses")
    plt.ylabel("Valor")

    plt.plot(periodo_arr, valor_arr)
    plt.show()