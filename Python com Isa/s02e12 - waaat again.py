turma = {}
somaaltura = float()

def alunosforadamedia():
	global turma
	aluno = input('Digite o nome da(o) aluna(o): ')
	if 'sair' in aluno.lower():
		print('(Pernalonga) Diga tchau, Lilica!\n(Lilica) Tchau Lilica!')
		exit(0)
	altura = float(input('Digite a altura desta(e) aluna(o): ').replace(',','.'))
	turma[aluno] = altura
	global somaaltura
	somaaltura += altura
	meidaaltura = somaaltura / len(turma)
	for valornome, valoraltura in turma.items():
		if valoraltura > meidaaltura:
			print(f'{valornome} tem {valoraltura}m e é mais alta que a média ({meidaaltura}m) das alunas.')

	alunosforadamedia()

alunosforadamedia()
