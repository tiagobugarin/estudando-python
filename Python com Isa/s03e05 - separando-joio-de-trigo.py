def joioetrigo():
    tupla = (5, 12, 1, 7, 88, 23, 54, 49, 122, 74, 447, 854, 33, 654)
    listaimpar = []
    listapar = []
    for i in tupla:
        if i % 2 == 0:
            listapar.append(i)
        else:
            listaimpar.append(i)
    tuplapar = tuple(listapar)
    print(f'A lista dos números ímpares é {listaimpar}.')
    print(f'A tupla dos números pares é {tuplapar}.')
