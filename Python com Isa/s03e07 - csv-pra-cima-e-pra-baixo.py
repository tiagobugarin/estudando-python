# isso tá muito ruim!! estudar mais

import requests
import pandas as pd
import io

csvurl = 'https://sites.google.com/site/dr2fundamentospython/arquivos/Winter_Olympics_Medals.csv'
urlData = requests.get(csvurl).content
tabela = pd.read_csv(io.StringIO(urlData.decode('utf-8')))
tabela = tabela.loc[tabela['Year'] >= 2001]
tabela = tabela.loc[tabela['Medal'] == 'Gold']
print(tabela)

# tudo ia bem até aqui mas foi pro brejo...

for pais in ['SWE', 'DEN', 'NOR']:
    for esporte in ['Curling', 'Skating', 'Skiing', 'Ice Hockey']:
        tabela = tabela.loc[tabela['NOC'] == pais]
        tabela = tabela.loc[tabela['Sport'] == esporte]
        if pais == 'SWE' and esporte == 'Curling':
            suicacurling = len(tabela)
        if pais == 'SWE' and esporte == 'Skating':
            suicaskating = len(tabela)
        if pais == 'SWE' and esporte == 'Skiing':
            suicaskiing = len(tabela)
        if pais == 'SWE' and esporte == 'Ice Hockey':
            suicaicehockey = len(tabela)
        if pais == 'DEN' and esporte == 'Curling':
            dinamarcacurling = len(tabela)
        if pais == 'DEN' and esporte == 'Skating':
            dinamarcaskating = len(tabela)
        if pais == 'DEN' and esporte == 'Skiing':
            dinamarcaskiing = len(tabela)
        if pais == 'DEN' and esporte == 'Ice Hockey':
            dinamarcaicehockey = len(tabela)
        if pais == 'NOR' and esporte == 'Curling':
            noruegacurling = len(tabela)
        if pais == 'NOR' and esporte == 'Skating':
            noruegaskating = len(tabela)
        if pais == 'NOR' and esporte == 'Skiing':
            noruegaskiing = len(tabela)
        if pais == 'NOR' and esporte == 'Ice Hockey':
            noruegaicehockey = len(tabela)

print(f'suicacurling é {suicacurling}')
print(f'dinamarcacurling é {dinamarcacurling}')
print(f'noruegacurling é {noruegacurling}')
print(f'suicaskating é {suicaskating}')
print(f'dinamarcaskating é {dinamarcaskating}')
print(f'noruegaskating é {noruegaskating}')
print(f'suicaskiing é {suicaskiing}')
print(f'dinamarcaskiing é {dinamarcaskiing}')
print(f'noruegaskiing é {noruegaskiing}')
print(f'suicaicehockey é {suicaicehockey}')
print(f'dinamarcaicehockey é {dinamarcaicehockey}')
print(f'noruegaicehockey é {noruegaicehockey}')

def comparacao(A, B, C):
    if A != B and B != C and A != C:
        if A > B:
            if A > C:
                print('A maior que B ou C')
            elif C > A:
                print('C maior que A ou B')
            else:
                print('B maior que A ou C')
        elif B > A:
            if B > C:
                print('B maior que A ou C')
            elif C > B:
                print('C maior que A ou B')
    elif A == B:
        if A > C:
            print('A e B são iguais e maiores que C')
        elif C > A:
            print('C maior que A ou B')
    elif A == C:
        if A > B:
            print('A e C são iguais e maiores que B')
        elif B > A:
            print('B maior que A ou C')
    elif B == C:
        if B > A:
            print('B e C são iguais e maiores que A')
        elif A > B:
            print('A maior que B ou C')

print('Curling')
comparacao(suicacurling, dinamarcacurling, noruegacurling)
print('Skating')
comparacao(suicaskating, dinamarcaskating, noruegaskating)
print('Skiing')
comparacao(suicaskiing, dinamarcaskiing ,noruegaskiing)
print('Ice Hockey')
comparacao(suicaicehockey, dinamarcaicehockey, noruegaicehockey)
