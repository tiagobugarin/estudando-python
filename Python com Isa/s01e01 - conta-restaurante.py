def conta_restaurante():

  qtd_pessoas = int(input("Quantas pessoas consumiram? "))
  while qtd_pessoas <= 0:
    print("A quantidade de consumidores não pode ser menor ou igual a 0, nem pode ser uma fração.")
    qtd_pessoas = int(input("Quantas pessoas consumiram? "))

  valor_conta = float(input("Qual o valor da conta? ").replace(',','.'))
  while valor_conta <= 0:
    print("Valor Inválido. O valor não pode ser menor ou igual a 0 reais.")
    valor_conta = float(input("Qual o valor da conta? ").replace(',','.'))

  taxa_servico = float(input("Qual o valor da taxa de serviço? ").replace(',','.'))
  while taxa_servico < 0 or taxa_servico > 100:
    print("Valor Inválido. O valor não pode ser menor que 0 reais ou maior que 100 reais.")
    taxa_servico = float(input("Qual o valor da taxa de serviço? ").replace(',','.'))

  media_pessoa = (valor_conta/qtd_pessoas)
  valor_final_pessoa = media_pessoa + (media_pessoa * taxa_servico / 100)
  txt_valor_final_pessoa = str(valor_final_pessoa).replace('.',',')

  print("Cada um deve pagar " , txt_valor_final_pessoa , "reais")