import pygame
import random

gameLoop = True
cor_fundo = [244, 140, 6, 255]
cor_vermelha = [220, 47, 2, 255]
cor_amarela = [255, 186, 8, 255]
janela_x = 800
janela_y = 480

pygame.init()
display = pygame.display.set_mode([janela_x, janela_y])
pygame.display.set_caption('pygame, pygame, quadrados memordam')
tipografia = pygame.font.SysFont("Calibri", 24)

x_vermelho = 50
y_vermelho = 50
w_vermelho = 100
h_vermelho = 100
x_amarelo = int()
y_amarelo = int()
w_amarelo = 50
h_amarelo = 50

def pixelamarelo():
    limitex = janela_x - w_amarelo + 1
    lancex = random.randint(0, limitex)
    limitey = janela_y - h_amarelo + 1
    lancey = random.randint(0, limitey)
    return lancex, lancey

if __name__ == '__main__':
    while gameLoop:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gameLoop = False

        display.fill(cor_fundo)

        keys = pygame.key.get_pressed()


        if keys[pygame.K_RIGHT]:
            x_vermelho += +1
        if keys[pygame.K_LEFT]:
            x_vermelho += -1
        if keys[pygame.K_SPACE]:
            x_amarelo = pixelamarelo()[0]
            y_amarelo = pixelamarelo()[1]
            quadradoamarelo = pygame.draw.rect(display, cor_amarela, pygame.Rect(x_amarelo, y_amarelo, w_amarelo, h_amarelo))

        
        quadradovermelho = pygame.draw.rect(display, cor_vermelha, pygame.Rect(x_vermelho, y_vermelho, w_vermelho, h_vermelho))
        

        pygame.display.update()
