lista = ['5', '123', '7', '52', '293', '2222', '55', '400', '752']

def trabalho_de_lista():
	print(f'As operações disponíveis são:\n(a) Mostrar a lista.\n(b) Incluir um elemento na lista.\n(c) Remover um elemento da lista.\n(d) Apagar todos os elementos da lista.\n(q) para sair do programa.')
	opcao = str(input('Qual operação deseja realizar? (a, b, c, d ou q) '))
	if opcao.lower() in 'abcd':
		if opcao.lower() == 'a':
			print(f'A lista é {lista}.')
		if opcao.lower() == 'b':
			novo = input('Digite um elemento pra incluir na lista: ')
			lista.append(novo)
		if opcao.lower() == 'c':
			remover = input('Digite um elemento pra remover da lista: ')
			lista.remove(remover)
		if opcao.lower() == 'd':
			lista.clear()
	elif opcao.lower() == 'q':
		print('Até mais!')
		exit(0)
	else:
		print('Opção escolhida não existe. Tente novamente.')
	trabalho_de_lista()

trabalho_de_lista()