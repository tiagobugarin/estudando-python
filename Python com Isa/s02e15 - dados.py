import numpy as np

todososlances = np.random.rand(1,100) # criando array de 1 dimensão e 100 elementos
todososlances = np.multiply(todososlances, 6) # tratando lançamentos anteriores para termos valores menores que 6
todososlances = np.ceil(todososlances) # arredondando valores pra cima, daí teremos apenas os números inteiros 1, 2, 3, 4, 5 e 6
print(f'{todososlances}') # imprime a array inteira

for i in range(1,7): # inicia um loop atribuindo a i os valores 1, 2, 3, 4, 5 e 6 a cada iteração
	quantidade = np.count_nonzero(todososlances == i) # conta quantas instâncias daquele valor i foram encontradas na array
	print(f'{i} aparece {quantidade} vezes.') # imprime bunitinho :) informando quantas vezes cada valor foi encontrado na array
